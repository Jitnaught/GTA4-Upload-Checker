# GTA4-Upload-Checker

# THIS PROGRAM IS NOW DEFUNCT. GTA4-Mods.com has been shut down, and now only an archive exists. This program will be kept here for historical purposes.

Checks if a GTA4-Mods.com author has uploaded a mod.

To use it you have to input the author's mod page, like this: http://www.gtagaming.com/downloads/author/247598

If the author doesn't have at least one uploaded and approved mod this program won't work.

You can also use the INI file so you don't have to input the link each time you start the program.

Requires libcurl. Compiles with GNU GCC (but probably works with others).
