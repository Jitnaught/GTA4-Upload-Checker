#include <string>
#include <iostream>
#include <conio.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <sys/stat.h>

using namespace std;

//void pressAnyKey()
//{
//    cout << "Press any key to continue.";
//    _getch();
//}

int strOccurances(string base, string findStr)
{
    int occurrences = 0;
    string::size_type start = 0;

    while ((start = base.find(findStr, start)) != string::npos)
    {
        ++occurrences;
        start += findStr.length();
    }

    return occurrences;
}

inline bool fileExists (const string& name)
{
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

char* iniReadStr(const char* section, const char* key, const char fileName[255])
{
    char* result = new char[255];
    memset(result, 0x00, 255);
    GetPrivateProfileString(section, key, "", result, 255, fileName);
    return result;
}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

int main()
{
    cout << "Hello!" << endl << endl;

    string authorPage = "";

    if (fileExists("./settings.ini"))
    {
        cout << "INI file exists." << endl << endl;
        authorPage = iniReadStr("SETTINGS", "LINK", "./settings.ini");
    }
    else cout << "INI file doesn't exist." << endl << endl;

    if (authorPage == "")
    {
        cout << "Author page not set in INI file." << endl << endl << "Please write the author page you want to check: ";
        cin >> authorPage;
    }
    else
    {
        cout << "Author page from INI file: \"" << authorPage << "\"." << endl << endl;
    }

    CURL* curl;
    CURLcode error;
    string readBuffer;

    byte errorCount = 0;
    string lastStr = "";
    bool approved = true;
    bool firstRun = true;

    curl = curl_easy_init();

    if (curl)
    {
        while (true)
        {
            if (errorCount > 10)
            {
                cout << "Too many errors occurred. Make sure you are connected to the internet and that you input the correct URL." << endl << endl;
                if (curl) curl_easy_cleanup(curl);
                return 3;
            }

            readBuffer = "";

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
            curl_easy_setopt(curl, CURLOPT_URL, authorPage.c_str());
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);

            if (!firstRun && errorCount == 0) Sleep(20000);

            error = curl_easy_perform(curl);

            const char *str = curl_easy_strerror(error);\

            cout << "Error returned: \"" << str << "\"." << endl << endl;

            if (error != 0)
            {
                errorCount++;
            }
            else
            {
                size_t i1 = readBuffer.find("Newest Mod");

                if (i1 == string::npos)
                {
                    cout << "Didn't find a key part of a GTA4-Mods.com author page. Are you sure you input the correct URL? If not, restart the application and input the correct URL." << endl << endl;
                    firstRun = false;
                    errorCount++;
                    continue;
                }
                else if (firstRun)
                {
                    cout << "Verified as a GTA4-Mods.com author page (unless you input a URL that contains the string that the program searches for.)" << endl << endl;
                }

                errorCount = 0;

                size_t i2 = readBuffer.find("</a>", i1);

                if (i2 == string::npos)
                {
                    firstRun = false;
                    continue;
                }

                string sub = readBuffer.substr(i1, i2 - i1);

                i1 = sub.find("<a href=\"");

                if (i1 == string::npos)
                {
                    firstRun = false;
                    continue;
                }

                sub = sub.substr(i1);

                i1 = sub.find(">");

                if (i1 == string::npos)
                {
                    firstRun = false;
                    continue;
                }

                sub = sub.substr(i1 + 1);

                cout << "Latest mod file name: \"" << sub << "\"." << endl << endl;

                int occurs = strOccurances(readBuffer, sub);

                if (!approved && occurs > 1)
                {
                    cout << "Newly added/approved mod!!!" << endl << endl;

                    for (int i = 0; i < 10; i++)
                    {
                        Beep(750, 300);
                        Sleep(100);
                    }
                    return 0;
                }
                else if ((lastStr != "" && sub != lastStr) || occurs == 1)
                {
                    approved = false;
                    cout << "Found unapproved mod." << endl << endl;
                }
                else if (occurs >= 2)
                {
                    string temp = string("It is not newly added/approved") + (firstRun ? string(" (unless it was approved before the program started.)") : string("."));
                    cout << temp << endl << endl;
                }

                lastStr = sub;
            }

            firstRun = false;
        }

        curl_easy_cleanup(curl);
    }
    else return 2;

    return 1;
}
